# Dockerfile for the extremely simple publisher that creates files in AWS S3 buckets
FROM python:3.11-slim-bookworm

RUN pip install poetry==1.8.3


ENV POETRY_NO_INTERACTION=1 \
    POETRY_VIRTUALENVS_IN_PROJECT=1 \
    POETRY_CACHE_DIR=/tmp/poetry_cache \
    POETRY_VIRTUALENVS_CREATE=1 \
    PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1


COPY pyproject.toml poetry.lock ./
RUN poetry install --with s3 --without dev,sftp --no-root
ENV PATH="/.venv/bin:$PATH"

EXPOSE 8090

WORKDIR /app
COPY ./publisher_s3 /app

RUN adduser -u 5678 --disabled-password --gecos "" appuser && chown -R appuser /app
USER appuser

CMD poetry run uvicorn --host 0.0.0.0 --port ${PUBLISHER_PORT:-8090} --log-config publisher_logging.conf main:app
