# Dockerfile for the SFTP publisher
FROM python:3.11-slim-bookworm

RUN pip install poetry==1.8.3


ENV POETRY_NO_INTERACTION=1 \
    POETRY_VIRTUALENVS_IN_PROJECT=1 \
    POETRY_CACHE_DIR=/tmp/poetry_cache \
    POETRY_VIRTUALENVS_CREATE=1 \
    PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1


WORKDIR /app

COPY pyproject.toml poetry.lock ./
RUN poetry install --with sftp --without dev,s3 --no-root

EXPOSE 8090

COPY ./publisher_sftp /app/publisher_sftp

ENV PATH="/.venv/bin:$PATH"

RUN adduser -u 5678 --disabled-password --gecos "" appuser && chown -R appuser /app
USER appuser

CMD  poetry run uvicorn --host 0.0.0.0 --port ${PUBLISHER_PORT:-8090} publisher_sftp.main:app
