# Publisher Backend

This repository contains code to set up a Publisher backend as microservice.

## Content

- [Backend description](#backend-description)
  + [SFTP Publisher](#sftp-publisher)
  + [S3 publisher](#s3-publisher)
- [Coding standards](#coding-standards)
  + [Formatting](#formatting)
  + [Organizing imports](#organizing-imports)
  + [IDE support](#ide-support)
  +  [Using pre-commit](#using-pre-commit)
- [Licence](#licence)


# Backend description

This repository contains the code to set up, develop and deploy the Publisher backend. (TODO: Continue the description)

## SFTP Publisher
The SFTP publisher comes with a simple FastAPI endpoint that sends the incoming messages as files to remote hosts via SFTP. The SFTP publisher can be configured with multiple remote hosts to send the files to. It is configured using `SERVERS` environment variable that contains a JSON-encoded string. A minimum configuration JSON string looks like this:

```
[{"name": "local-server", "username": "opmet", "password": "pass", "hostname": "sftp-server", "remote_dir": "/output"}]
```

You can add multiple servers to the array, see the example in the [.env](publisher_sftp/.env)-file. The files will be sent to all of them. See the Settings class in [publisher_sftp/main.py](publisher_sftp/main.py) for the full configuration schema.

You can start the service locally from the main directory with:
```
docker build -t publisher -f sftp.Dockerfile .
docker run -d -it --env-file publisher_sftp/.env --name publisher --network host publisher
```
You could add extra arguments to the `docker run` command so that it runs on the correct network for example.

You can check that the service is healthy by navigating to [http://0.0.0.0:8090/healthcheck](http://0.0.0.0:8090/healthcheck), with port 8090 configured as the default port number. It should return `"status":"OK","service":"SFTP publisher"}`. The healthcheck caches results by default. You can use `HEALTHCHECK_CACHE_SECONDS` environment variable to configure the duration or set it to 0 to disable caching.

Additionally, you can check out the logs with `docker logs -f publisher`.

## S3 publisher

The included publisher_s3 comes with an extremely simple FastAPI endpoint that stores the incoming message as a text file into an Amazon S3 bucket defined by S3_BUCKET_NAME environment variable. Note that the necessary AWS credentials need to be provided to the container by whatever method is approppriate in the used deployment environment for it to work. Please note that the s3_publisher works only when deployed on AWS.

# Local installation

## 1. Install Pyenv with Pyenv installer

Check the [prerequisites](https://github.com/pyenv/pyenv/wiki#suggested-build-environment) from the pyenv wiki. for Debian Linux they are:

```bash
sudo apt update; sudo apt install build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev curl libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev
```

Run the Pyenv installer script:

```bash
curl https://pyenv.run | bash
```

Setup your shell environment with these [instructions](https://github.com/pyenv/pyenv#set-up-your-shell-environment-for-pyenv)

## 2. Build and install Python

With Pyenv installed, check the available Python versions with

```bash
pyenv install -l
```

Install the latest Python 3.11 version from the list and set it as global Python version for your system.

```bash
pyenv install 3.11.8
pyenv global 3.11.8
```

## 3. Install Pipx

Pipx is a helper utility for installing system wide Python packages, adding them to your path automatically.

Install pipx with these [instructions](https://pipx.pypa.io/latest/installation/). Linux users can install it with pip:

```bash
python3 -m pip install --user pipx
python3 -m pipx ensurepath

# Optional
sudo pipx ensurepath --global
```

## 4. Install Poetry

Poetry is used for managing project dependencies and metadata. Install Poetry with

```bash
pipx install poetry
```

and add poetry export plugin to it

```bash
pipx inject poetry poetry-plugin-export
```

## 5. Activate virtualenv and install dependencies

Activate the virtualenv with poetry

```bash
poetry shell
```

And install project dependencies

```bash
poetry install
```

# Managing Python dependencies

With Poetry installed from previous step, you can add or update Python dependencies with poetry add command.

```bash
# adding dependency
poetry add fastapi

# updating dependency
poetry update fastapi
```

You can add development dependencies with --group keyword.

```bash
# adding development dependency
poetry add --group dev isort
```

All poetry operations respect version constraints set in pyproject.toml file.

## Fully updating dependencies

You can see a list of outdated dependencies by running

```bash
poetry show --outdated
```

To update all dependencies, still respecting the version constraints on pyproject.toml, run

```bash
poetry update
```

# Dependabot integration

This repository has a Dependabot integration with the following behavior specified in the `.gitlab/dependabot.yml` file:

| definition               | behavior                                                                                                                                                                                                                                                                   |
|--------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| schedule block           | Scan the repository daily at random time between 6-11. Add any pending package update as MR and update any MR with merge conflicts by doing a rebase. If there is a newer release of the dependency that already has an open MR, make a new MR and close the previous one. |
| open-pull-requests-limit | Don't open more than 3 MRs per directory, even if there are more package updates available than 3.                                                                                                                                                                         |
| rebase-strategy          | Always rebase open MRs that have dependabot as assignee, if there are merge conflicts present.                                                                                                                                                                             |
| assignee                 | add dependabot as a assignee for every MR it opens.                                                                                                                                                                                                                        |
| ignore block             | Ignore boto3, boto3-stubs and mypy-boto3 patch updates, as new version is released almost daily. These can be updated with `poetry update` command on demand.                                                                                                              |

More detailed guide on how to work with dependabot can be found in the [opengeoweb wiki](https://gitlab.com/groups/opengeoweb/-/wikis/How-to-work-with-Dependabot-MRs)


# Coding standards

## Formatting

This project follows the [Google Python Style Guide](https://github.com/google/styleguide/blob/gh-pages/pyguide.md). The [YAPF](https://github.com/google/yapf) tool can be used to enforce this. If you installed all dependencies using `pip install -r requirements-test.txt -r requirements-dev.txt`, you should be able to run YAPF by executing `yapf -ir app/`. This will format all Python files in [app](app), using the configuration specified in [.style.yapf](.style.yapf).

For static typing checks, use mypy by executing `mypy app`.

## Organizing imports

This project uses [isort](https://pycqa.github.io/isort/) for organizing import statements. This is a tool to sort imports alphabetically, and automatically separates them into sections and by type. To sort imports for the project run `isort app/`.

## IDE support

If you are using Visual Studio Code, consider updating [.vscode/settings.json](.vscode/settings.json) with the contents below. This will format code and organize imports on save.

```
{
    "editor.codeActionsOnSave": {
        "source.organizeImports": true
    },
    "python.formatting.provider": "yapf",
    "[python]": {
        "editor.formatOnSave": true
    }
}
```

## Using pre-commit

You can also use the pre-commit configuration. This is used to inspect the code that's about to be committed, to see if you've forgotten something, to make sure tests run, or to examine whatever you need to inspect in the code. To use pre-commit make sure the `pip install` command has succesfully ran with the `requirements-dev.txt` argument.
Then execute the following command in the terminal in the root folder:
 ```
 pre-commit install
 ```
 This will install the pre-commit functions and now pre-commit will run automatically on each `git commit`.
 Only changed files are taken into consideration.

 Optionally you can check all the files with the pre-commit checks (NOTE: this command can make changes to files):

```
pre-commit run --all-files
```

If you wish to skip the pre-commit test but do have pre-commit setup, you can use the `--no-verify` flag in git to skip the pre-commit checks. For example:
```
git commit -m "Message" --no-verify
```

# Licence

This project is licensed under the terms of the Apache 2.0 licence.
Every file in this project has a header that specifies the licence and copyright. It is possible to check/add/remove the licence header using the following commands:

`npm run licence:check` => checks all the files for having the header text as specified in the LICENCE file. The format and which files to include is specified in `licence.config.json`.

`npm run licence:add` => adds licence to files without it. This action can require a manual check when the file contains a wrong header (i.g. a simple word is missed), because it just adds another header without removing the wrong one.

`npm run licence:remove` => removes licence from all files. This action can require a manual check when the file contains a wrong header (i.g. a simple word is missed), because it only removes a correct header and not a wrong one.


# Unit testing

To run unit testing using pytest (including coverage) you can execute `pytest app  --cov .. --cov-config=.coveragerc .`.
