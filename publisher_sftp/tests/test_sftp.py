# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)

# pylint: disable=redefined-outer-name,unused-argument
"""Basic testing for SFTP Publisher"""
import json
import logging
import os
import re
from collections.abc import Generator
from datetime import datetime, timedelta
from unittest.mock import patch

import paramiko
import pytest
from fastapi import FastAPI
from fastapi.testclient import TestClient

from publisher_sftp.main import app as tst_app
from publisher_sftp.server import (Server, connect_to_server,
                                   filename_pattern_matches,
                                   send_file_to_server)
from publisher_sftp.settings import settings

# Set logging level to INFO
logging.getLogger().setLevel(logging.INFO)


@pytest.fixture
def app() -> Generator[FastAPI]:
    """Yields the application configured in testing mode"""

    yield tst_app


@pytest.fixture
def client(app: FastAPI) -> Generator[TestClient]:
    """Yields a test client"""

    yield TestClient(app)


@pytest.fixture(scope="session")
def docker_compose_file(pytestconfig):
    """Override pytest-docker default docker-compose file location"""
    return os.path.join(str(pytestconfig.rootdir), "ssh-compose.yml")


def is_responsive(server):
    """Check if ssh target docker service is responsive"""
    try:
        client = paramiko.SSHClient()
        connect_to_server(client, server)
        return True
    except Exception:
        return False


@pytest.fixture(scope="session")
def sftp_service(docker_ip, docker_services):
    """Ensure that HTTP service is up and responsive."""

    # `port_for` takes a container port and returns the corresponding host port
    port = docker_services.port_for("local-ssh-server", 22)
    server = Server(name="local-server",
                    username="root",
                    password="geoweb",
                    hostname=docker_ip,
                    port=port,
                    remote_dir="/tmp")
    docker_services.wait_until_responsive(timeout=30.0,
                                          pause=0.1,
                                          check=lambda: is_responsive(server))
    return server


def test_healthcheck(client: TestClient, sftp_service):
    """Test the app health check"""
    settings.servers = [sftp_service]
    resp = client.get('/healthcheck')

    assert resp.json() == {
        'status': 'OK',
        'service': 'SFTP publisher',
        'servers': [{
            "host": sftp_service.hostname,
            "status": "OK"
        }]
    }


@patch('paramiko.SSHClient.open_sftp')
@patch('paramiko.SSHClient.connect')
@patch('publisher_sftp.healthcheck.datetime')
@patch('publisher_sftp.main.settings', autospec=True)
def test_healthcheck_caching(mock_settings, mock_datetime, mock_connect,
                             mock_open_sftp, client: TestClient):
    """Test healthcheck caching behavior"""
    test_server = Server(name='TestServer',
                         username='testuser',
                         password='testpassword',
                         hostname='testhostname',
                         port=22,
                         remote_dir='/test/remote_dir')
    mock_settings.servers = [test_server]

    base_time = datetime(2025, 1, 1, 12, 0)
    mock_datetime.now.return_value = base_time

    # Cache miss
    response1 = client.get('/healthcheck')
    assert mock_connect.call_count == 1

    # Cache hit
    response2 = client.get('/healthcheck')
    assert response2.json() == response1.json()
    assert mock_connect.call_count == 1

    # Cache miss
    mock_datetime.now.return_value = base_time + timedelta(seconds=40)
    response3 = client.get('/healthcheck')
    assert response3.json() == response1.json()
    assert mock_connect.call_count == 2


@patch('paramiko.SSHClient.connect')
@patch('paramiko.SSHClient.open_sftp')
@patch('paramiko.SFTP.putfo')
@patch('paramiko.SFTP.rename')
def test_send_file_to_server_success_use_temp(mock_rename, mock_putfo,
                                              mock_open_sftp, mock_connect,
                                              caplog):
    """ Test the publishing with a temp file """
    server = Server(name='TestServer',
                    username='testuser',
                    password='testpassword',
                    hostname='testhostname',
                    port=22,
                    remote_dir='/test/remote_dir',
                    timeout_seconds=10,
                    use_temp_file=True,
                    temp_file_suffix=".tmp")

    with open('publisher_sftp/tests/testdata1.json', 'rb') as fh:
        filename1 = "example.json"
        data = json.dumps(json.load(fh))
        result = send_file_to_server(server, filename1, data)
        assert 'message' in result
        assert result['message'] == filename1 + " published to TestServer"
        assert 'error' not in result
        assert "Published " + filename1 + " to TestServer" in caplog.text


@patch('paramiko.SSHClient.connect')
@patch('paramiko.SSHClient.open_sftp')
@patch('paramiko.SFTP.putfo')
def test_send_file_to_server_success_no_temp(mock_putfo, mock_open_sftp,
                                             mock_connect, caplog):
    """ Test the publishing without a temp file """
    server = Server(name='TestServer',
                    username='testuser',
                    password='testpassword',
                    hostname='testhostname',
                    port=22,
                    remote_dir='/test/remote_dir',
                    timeout_seconds=10,
                    use_temp_file=False)

    with open('publisher_sftp/tests/testdata1.json', 'rb') as fh:
        filename1 = "example.json"
        data = json.dumps(json.load(fh))
        result = send_file_to_server(server, filename1, data)
        assert 'message' in result
        assert result['message'] == f"{filename1} published to {server.name}"
        assert 'error' not in result
        assert f"Published {filename1} to {server.name}" in caplog.text


@patch('paramiko.SSHClient.connect', side_effect=Exception('Connection failed'))
def test_send_file_to_server_failure(mock_connect, caplog):
    """ Test the handling of a failed connection """
    server = Server(name='TestServer',
                    username='testuser',
                    password='testpassword',
                    hostname='testhostname',
                    port=22,
                    remote_dir='/test/remote_dir',
                    timeout_seconds=10,
                    use_temp_file=True,
                    temp_file_suffix=".tmp")
    with open('publisher_sftp/tests/testdata1.json', 'rb') as fh:
        filename1 = "example.json"
        data = json.dumps(json.load(fh))
        result = send_file_to_server(server, filename1, data)
    assert 'error' in result
    assert 'message' not in result
    # Check the error is logged
    expected_log_message = f"Failed to publish {filename1} to {server.name}"
    assert any(expected_log_message in log for log in caplog.messages)


def test_send_file(mocker, caplog, client: TestClient):
    """ Test the call that will publish to an array of servers """
    servers = [
        Server(name='TestServer',
               username='testuser',
               password='testpassword',
               hostname='testhostname',
               port=22,
               remote_dir='/test/remote_dir',
               timeout_seconds=10,
               use_temp_file=True,
               temp_file_suffix=".tmp")
    ]

    with patch('publisher_sftp.main.settings', autospec=True) as mock_settings:
        # Set the return value of the mock_settings attribute
        mock_settings.servers = servers

        mocker.patch('paramiko.SSHClient.connect')
        mocker.patch('paramiko.SSHClient.open_sftp')

        with open('publisher_sftp/tests/testdata1.json', 'rb') as fh:
            filename1 = "example.json"
            data = json.dumps(json.load(fh))

            # Make a request to the send_file endpoint
            response = client.post("/publish/" + filename1,
                                   json={"data": data},
                                   headers={"Content-Type": "application/json"})

            # Assert the response status code
            assert response.status_code == 200

            # Assert the log messages
            expected_log_message = f"Published {filename1} to {servers[0].name}"
            assert expected_log_message in caplog.text


def test_filename_pattern_matches_no_pattern():
    """ Test that a file matches when no pattern is provided"""
    server = Server(name='TestServer',
                    username='testuser',
                    hostname='testhostname',
                    remote_dir='/test/remote_dir')
    filename = 'any_file.txt'
    assert filename_pattern_matches(server, filename)


def test_filename_pattern_matches_pattern():
    """ Test that a file matches/doesn't match when a pattern is provided"""
    pattern = re.compile(r'^test_.*\.txt$')
    server = Server(name='TestServer',
                    username='testuser',
                    hostname='testhostname',
                    remote_dir='/test/remote_dir',
                    filename_pattern=pattern)
    filename1 = 'test_example.txt'
    assert filename_pattern_matches(server, filename1)
    filename2 = 'example.jpg'
    assert not filename_pattern_matches(server, filename2)
