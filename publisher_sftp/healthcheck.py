# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
"""SFTP publisher healthcheck models and functions"""

import logging
import os
import socket
import uuid
from dataclasses import dataclass
from datetime import datetime
from io import BytesIO

import paramiko
from paramiko.ssh_exception import SSHException

from publisher_sftp.server import Server, create_sftp_client
from publisher_sftp.settings import settings

logger = logging.getLogger(__name__)


@dataclass
class ServerHealthCheck:
    """DTO for healthcheck operations"""
    server: Server
    healthy: str = "UNKNOWN"
    error: str | None = None
    error_type: str | None = None

    def message(self) -> dict:
        """Returns a dictionary with status information"""
        msg = {"host": self.server.hostname, "status": self.healthy}
        if self.error is not None:
            msg["error"] = self.error
            msg["status"] = "FAILED"
            msg["error-type"] = self.error_type or "UNKNOWN"
        return msg


@dataclass
class CacheEntry:
    """Server healthcheck cache entry"""
    result: ServerHealthCheck
    timestamp: datetime


class HealthcheckCache:
    """Cache for server healthcheck results"""

    def __init__(self):
        self.cache: dict[str, CacheEntry] = {}

    def get_result(self, server: Server,
                   cache_seconds: int) -> ServerHealthCheck | None:
        """Get cached result if valid based on cache_seconds"""
        if cache_seconds <= 0:
            return None
        if not (entry := self.cache.get(server.hostname)):
            return None
        if (datetime.now() - entry.timestamp).total_seconds() >= cache_seconds:
            return None
        return entry.result

    def update(self, result: ServerHealthCheck) -> None:
        """Update cache with new result"""
        self.cache[result.server.hostname] = CacheEntry(result, datetime.now())


healthcheck_cache = HealthcheckCache()


def write_healthcheck_file(sftp_client: paramiko.SFTPClient, remote_path: str):
    """Write healthcheck file in sftp target and delete it"""
    try:
        sftp_client.remove(remote_path)
    except FileNotFoundError:
        logger.info("No previous healthcheck file found")
    buffer = BytesIO(bytes("healthcheck", "utf-8"))
    logger.info("writing healthcheck file")
    sftp_client.putfo(buffer, remote_path)
    sftp_client.remove(remote_path)
    logger.info("healthcheck file write and delete successful")


def healthcheck_filename(extension: str = '.tmp') -> str:
    """Generate unique filename for healthcheck"""
    return f"health_{uuid.uuid4()}{extension}"


async def server_healthcheck(server: Server) -> ServerHealthCheck:
    """Check individual server healthcheck with caching"""
    if (cached :=
            healthcheck_cache.get_result(server,
                                         settings.healthcheck_cache_seconds)):
        return cached

    client = paramiko.SSHClient()
    try:
        sftp = create_sftp_client(client, server)
        remote_path = os.path.join(server.remote_dir, healthcheck_filename())
        write_healthcheck_file(sftp, remote_path)
        result = ServerHealthCheck(server, healthy="OK")
        healthcheck_cache.update(result)
        return result
    except (SSHException, socket.error) as e:
        logger.error(e)
        result = ServerHealthCheck(server,
                                   healthy="FAILED",
                                   error=str(e),
                                   error_type=str(type(e)))
        healthcheck_cache.update(result)
        return result
    finally:
        client.close()
