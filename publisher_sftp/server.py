# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
"""SFTP publisher server model and functions"""

import logging
import os
from io import BytesIO
from re import Pattern

import paramiko
from pydantic import BaseModel

logger = logging.getLogger(__name__)


class Server(BaseModel):
    """Server settings"""
    name: str
    username: str
    password: str | None = None  # None if using key auth
    private_key_path: str | None = None  # None if using password auth
    private_key_passphrase: str | None = None  # None if using password auth
    hostname: str
    port: int = 22
    remote_dir: str
    timeout_seconds: int = 10
    use_temp_file: bool = True
    temp_file_suffix: str = ".tmp"
    filename_pattern: Pattern | None = None  # None if the server handles all files


def create_sftp_client(client: paramiko.SSHClient,
                       server: Server) -> paramiko.SFTPClient:
    """Creates SFTP client from paramiko SSHClient"""
    try:
        connect_to_server(client, server)
        sftp = client.open_sftp()
        return sftp
    except Exception as ex:
        raise ex


def connect_to_server(client: paramiko.SSHClient,
                      server: Server) -> paramiko.SSHClient:
    """Connects client to the server"""
    try:
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(hostname=server.hostname,
                       port=server.port,
                       username=server.username,
                       password=server.password,
                       key_filename=server.private_key_path,
                       passphrase=server.private_key_passphrase,
                       timeout=server.timeout_seconds,
                       auth_timeout=server.timeout_seconds,
                       banner_timeout=server.timeout_seconds,
                       channel_timeout=server.timeout_seconds)

        return client
    except Exception as ex:
        raise ex


def filename_pattern_matches(server: Server, filename: str):
    """Check if filename matches the pattern.
       Returns true if the server has no filename pattern set"""
    if server.filename_pattern is None:
        return True
    return server.filename_pattern.match(filename) is not None


def send_file_to_server(server: Server, filename: str, data: str):
    """Function that takes filename and content as a string and sends it as a
       file to a remote host via SFTP"""
    remote_path = os.path.join(server.remote_dir, filename)
    client = paramiko.SSHClient()

    try:
        sftp = create_sftp_client(client, server)
        buffer = BytesIO(bytes(data, "utf-8"))
        if server.use_temp_file:
            temp_path = remote_path + server.temp_file_suffix
            sftp.putfo(buffer, temp_path)
            sftp.rename(temp_path, remote_path)
        else:
            sftp.putfo(buffer, remote_path)
        logger.info("Published %s to %s", filename, server.name)
        return {"message": f"{filename} published to {server.name}"}
    except Exception as ex:
        logger.error("Failed to publish %s to %s: %s", filename, server.name,
                     ex)
        return {"error": f"Failed to publish {filename} to {server.name}"}
    finally:
        client.close()
