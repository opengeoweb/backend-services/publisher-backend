# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
"""SFTP publisher microservice"""

import asyncio
import logging
import sys
from concurrent.futures import ThreadPoolExecutor

from fastapi import Body, FastAPI, HTTPException

from publisher_sftp.healthcheck import server_healthcheck
from publisher_sftp.server import filename_pattern_matches, send_file_to_server
from publisher_sftp.settings import settings

logging.basicConfig(stream=sys.stdout,
                    level=logging.INFO,
                    format="[%(levelname)s] %(name)s: %(lineno)d %(message)s")
logger = logging.getLogger(__name__)

app = FastAPI()


@app.post("/publish/{filename}")
def send_file(filename: str, data: str = Body(min_length=4, embed=True)):
    """Endpoint that takes filename and content as a string and sends it as a
       file to multiple remote hosts via SFTP in parallel"""
    servers = [
        server for server in settings.servers
        if filename_pattern_matches(server, filename)
    ]
    if not servers:
        logger.error(
            "Failed to publish %s, no servers with matching filename pattern",
            filename)
        raise HTTPException(status_code=500,
                            detail={"error": f"Failed to publish {filename}"})
    with ThreadPoolExecutor() as executor:
        results = list(
            executor.map(
                lambda server: send_file_to_server(server, filename, data),
                servers))
    if any("error" in result for result in results):
        raise HTTPException(status_code=500, detail=results)
    return results


@app.get('/healthcheck')
async def healthcheck():
    """Health check that can be called to see if service is accessible.

    Example:
        Successful response::

            {
                "status": "OK",
                "service": "SFTP publisher",
                "servers": [
                    {
                        "host": "localhost",
                        "status": "OK"
                    }
                ]
            }
    """
    servers = settings.servers

    tasks = []
    for server in servers:
        task = server_healthcheck(server)
        tasks.append(task)
    results = await asyncio.gather(*tasks)

    overall_status_msg = {
        'status': 'OK',
        'service': 'SFTP publisher',
        "servers": []
    }
    for check in results:
        message = check.message()
        if message["status"] == "FAILED":
            overall_status_msg["status"] = "FAILED"
        overall_status_msg["servers"].append(message)
    if overall_status_msg["status"] == "FAILED":
        raise HTTPException(503, detail=overall_status_msg)
    return overall_status_msg
