# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)

# pylint: disable=redefined-outer-name,unused-argument
"""Basic testing for S3 Publisher"""
import json
import os
from collections.abc import Generator

import boto3
import pytest
from fastapi import FastAPI, HTTPException
from fastapi.testclient import TestClient
from moto import mock_aws

import publisher_s3
from publisher_s3.main import app as tst_app
from publisher_s3.main import save_file

# Set up relevant env variables for testing of the S3 functionality
BUCKET_NAME = "publisher_bucket"
AWS_REGION = "eu-west-1"
os.environ["AWS_REGION"] = AWS_REGION
os.environ["S3_BUCKET_NAME"] = BUCKET_NAME


@pytest.fixture
def app() -> Generator[FastAPI]:
    """Yields the application configured in testing mode"""
    yield tst_app


@pytest.fixture
def fastapi_client(app: FastAPI) -> Generator[TestClient]:
    """Yields a test client"""

    yield TestClient(app)


@pytest.fixture()
def publish_bucket():
    """S3 bucket mocked using the moto library"""
    with mock_aws():
        s3_resource = boto3.resource('s3', region_name=os.environ['AWS_REGION'])
        yield s3_resource.create_bucket(Bucket=os.environ["S3_BUCKET_NAME"],
                                        CreateBucketConfiguration={
                                            'LocationConstraint':
                                                os.environ['AWS_REGION']
                                        })


@pytest.fixture()
def publish_bucket2():
    """Another S3 bucket mocked using the moto library"""
    with mock_aws():
        s3_resource = boto3.resource('s3', region_name=os.environ['AWS_REGION'])
        yield s3_resource.create_bucket(Bucket="ANOTHERNAME",
                                        CreateBucketConfiguration={
                                            'LocationConstraint':
                                                os.environ['AWS_REGION']
                                        })


def test_healthcheck(fastapi_client: TestClient):
    """Test the app health check"""

    resp = fastapi_client.get('/healthcheck')

    assert resp.json() == {'status': 'OK', 'service': 'S3 publisher'}


@pytest.mark.asyncio
async def test_file_published_on_s3(publish_bucket):
    """Testing the functionality when publishing goes as expected"""
    filename1 = "example.json"
    filename2 = "example2.json"
    with open('publisher_s3/tests/testdata1.json',
              'rb') as fh1, open('publisher_s3/tests/testdata2.json',
                                 'rb') as fh2:
        assert (len(list(publish_bucket.objects.all())) == 0)
        data1 = json.dumps(json.load(fh1))
        # response = client.post("/publish/example.json", data)
        await save_file(filename1, data1)
        # Check that one more file exists in the bucket
        assert (len(list(publish_bucket.objects.all())) == 1)
        # Publish another one
        data2 = json.dumps(json.load(fh2))
        await save_file(filename2, data2)
        assert (len(list(publish_bucket.objects.all())) == 2)
        # Check that the file names match the names provided
        assert len(list(publish_bucket.objects.filter(Prefix=filename1))) == 1
        assert len(list(publish_bucket.objects.filter(Prefix=filename2))) == 1
        # Check that the contents match the original json files
        publish_bucket.download_file(filename1, '/tmp/' + filename1)
        with open('/tmp/' + filename1, "r", encoding='utf-8') as p1:
            content = p1.read()
            assert data1 in content

        publish_bucket.download_file(filename2, '/tmp/' + filename2)
        with open('/tmp/' + filename2, "r", encoding="utf-8") as p2:
            content = p2.read()
            assert data2 in content


@pytest.mark.asyncio
async def test_nocredentialserror_on_publish():
    """Testing the exception when no credentials are provided"""
    # When no bucket fixture is used, there are no mocked credentials either
    filename = "example.json"
    with open('publisher_s3/tests/testdata1.json', 'rb') as fh:
        data = json.dumps(json.load(fh))

        result = await publisher_s3.main.save_file(filename, data)
        assert isinstance(result, HTTPException)
        assert 'Publishing to S3 bucket failed' in result.detail
        assert result.status_code == 500


@pytest.mark.asyncio
async def test_clientrror_on_publish(publish_bucket2):
    """Testing a generic ClientError"""
    # In this case, the existing bucket has a different name than expected
    filename = "example.json"
    with open('publisher_s3/tests/testdata1.json', 'rb') as fh:
        data = json.dumps(json.load(fh))

        result = await publisher_s3.main.save_file(filename, data)
        assert isinstance(result, HTTPException)
        assert result.detail == 'Publishing to S3 bucket failed: ClientError'
        assert result.status_code == 500
