# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
"""File saving microservice for AWS S3"""
import logging

import boto3
from botocore.exceptions import ClientError, NoCredentialsError
from fastapi import Body, FastAPI, HTTPException
from pydantic_settings import BaseSettings

logger = logging.getLogger(__name__)

app = FastAPI()


class Settings(BaseSettings):
    """Settings, read from ENV"""
    S3_BUCKET_NAME: str = "publisher_bucket"


settings = Settings()


@app.post("/publish/{filename}")
async def save_file(filename: str, data: str = Body(min_length=4, embed=True)):
    """Endpoint to take filename and string and save them into AWS S3 bucket"""
    # Create an S3 client using boto3
    s3_client = boto3.client("s3")

    logger.info("Start publishing %s to S3", filename)

    try:
        # Create the file in S3
        result = s3_client.put_object(Body=data,
                                      Bucket=settings.S3_BUCKET_NAME,
                                      Key=filename)
        # Check if successful
        if result['ResponseMetadata']['HTTPStatusCode'] == 200:
            logger.info("Successfully published %s in S3 bucket", filename)
            return {
                "message": f"File '{filename}' saved successfully to bucket."
            }

        # If not successful, notify in log and message
        logger.error("Publishing file into S3 bucket %s failed",
                     settings.S3_BUCKET_NAME)
        return HTTPException(400, detail='Publishing to S3 bucket failed')
    # Catch other exceptions
    except NoCredentialsError as err:
        logger.error("Error while connecting to bucket: no credentials found",
                     extra={"err": err})
        return HTTPException(
            500, detail="Publishing to S3 bucket failed: NoCredentialsError")
    except ClientError as err:
        logger.error("Failed publishing %s to S3 bucket",
                     filename,
                     extra={"err": err})
        return HTTPException(
            500, detail="Publishing to S3 bucket failed: ClientError")


@app.get('/healthcheck')
async def healthcheck():
    """Health check that can be called to see if service is accessible."""
    return {'status': 'OK', 'service': 'S3 publisher'}
